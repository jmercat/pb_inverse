all:
	gfortran -o 2D.exe parametres.f90 fonctions.f90 module_mu.f90 main.f90
debug :
	gfortran -fdefault-real-8 -O0 -g  -fbounds-check -Wall  -ffpe-trap=invalid,zero,overflow,underflow -fbacktrace -ftrapv -fimplicit-none  -o 2D.exe parametres.f90 fonctions.f90 module_mu.f90 main.f90
clean:
	rm *.mod *.o
