module


contains


subroutine discretisation
implicit none
integer::i,j
integer::L1,L2
real*8::dx1,dx2
real*8,dimension(:),allocatable::x,y

allocate(x(L1),y(L2))

do i=1,L1
	x(i)=dx1*(i-1)
end do

do j=1,L2
	y(j)=dx2*(j-1)
end do

end subroutine


function f(rk,theta_k) result(l)
implicit none

integer::i,j,n
integer::L1,L2,GN
real*8::l,pt,gt
real*8,dimension(:),allocatable::mu,x,y

allocate (mu(GN))

l=0.0
pt=0.0
gt=0.0


do i=1,L1
	do j=1,L2
		l=l+(rho1(x(i),y(j)))**2
		do n=1,GN
			l=l+rho1(x(i),y(j))*mu(n)*((2/eps)-(4/eps**2)*abs(x(i)-rk*cos(theta_k)))
			pt=pt+mu(n)*((2/eps)-(4/eps**2)*abs(x(i)-rk*cos(theta_k)))
		end do
		gt=pt**2
		l=l+gt
	end do
end do




end function


function df(rk,theta_k)
implicit none


do i=1,L1
	do j=1,L2
		do n=1,GN
			l=l+rho1(x(i),y(j))*mu(n)*((2/eps)-(4/eps**2)*abs(x(i)-rk*cos(theta_k)))
			pt1=pt1+mu(n)*(-4.0/eps**2)*(rk*sin(theta_k))*sign(x(i)-rk*cos(theta_k)))
			pt2=pt2+mu(n)*(-4.0/eps)*(rk*sin(theta_k)
			pt3=pt3+mu(n)*((2/eps)-(4/eps**2)*abs(x(i)-rk*cos(theta_k)))
		end do
		l=l+gt1+2*pt2*pt3
	end do
end do



end module
