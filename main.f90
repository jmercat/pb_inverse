program pb_inverse
	use parametres
	use fonctions
	use Mu_Resol
	implicit none

	
	integer::i,j,k
	!Vecteur de passage pour la descente / remontée
	real*8,dimension(Nk**2)::Y
	real*8::resultat
		
	allocate(X_part(Nk**2,2))
	!print*,eps
	!Initialisation de la position des particules
	call Init(X_part)
	call Affiche_Dirac(21)
	call Affiche_rho0()
	!Détermination des mu_i
	!Construction de la matrice A
	
	
	
	call Build_A(A)
	call cholesky(A,L)
	call second_membre(B)
	call descente(b,L,y)
	call remontee(L,Y,Mu_i)
	!Affiche la solution
	call Affiche_rho0_tilde()
	!Détermine l'erreur L2
	call Get_Error2_Rho0_Rho0Tilde(Mu_i,Arret_Critere) 
	Arret_Critere=Arret_Critere*2

	call solveXk(mu_i)
	
	call Affiche_rho0_tilde()


end program
