module parametres
	implicit none

	real*8,parameter::tol_gradconj=1D-5, itermax =1000, penalisation = 0D0
	!Nombre de points de discrétisation
	integer,parameter::Nk=9
	!Définition du vecteur X(x1,x2)
	real*8,dimension(:,:),allocatable::X_part
	!Parametre de discretisation de l'espace
	integer,parameter::N=50
	real*8,dimension(Nk**2,Nk**2)::A !matrice de masse pour les mu_i
	real*8,dimension(Nk**2,Nk**2)::L !matrice de factorisation de cholesky
	real*8,dimension(Nk**2)::B !second membre pour le calcul des mu_i
	real*8,dimension(Nk**2)::Mu_i
	real*8::Arret_Critere

	real*8::eps=0.8

end module
