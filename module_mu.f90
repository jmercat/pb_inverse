module Mu_Resol
  use fonctions
  use parametres
contains

  !Integrale du dirac i avec le dirac j
  subroutine integrale(i,j,resultat)
    implicit none
    integer,intent(in)::i,j
    integer::k,l
    real*8,dimension(2)::Coord
    real*8,intent(out)::resultat

    resultat=0
    do l=1,N
       Coord(2)=l*1./N
       do k=1,N
          Coord(1)=k*1./N 
          resultat=resultat+Dirac(Coord(:),X_part(i,:))*Dirac(Coord(:),X_part(j,:))*1./N**2
       end do
    end do


  end subroutine integrale

  subroutine Build_A(A)
    implicit none
    real*8,dimension(Nk**2,Nk**2),intent(inout)::A
    real*8::k
    integer::i,j

    A=0

    do i=1,Nk**2
       do j=1,Nk**2
          call integrale(i,j,k)	
          A(i,j)=k
       end do
    end do

  end subroutine Build_A

  subroutine second_membre(resultat)
    implicit none


    !~     interface
    !~        real*8 function f(x)
    !~          real*8,dimension(2)::x
    !~        end function f
    !~     end interface

    real*8,dimension(2)::X,Coord
    integer::i,k,l
    real*8,dimension(Nk**2)::resultat


    resultat=0
    do i=1,Nk**2
       do l=1,N
          Coord(2)=l*1./N
          do k=1,N
             Coord(1)=k*1./N 
             resultat(i)=resultat(i)+rho0(Coord(:))*Dirac(Coord(:),X_part(i,:))*1./N**2
          end do
       end do
    end do

  end subroutine second_membre


  subroutine cholesky(A,L)
    implicit none
    real*8,dimension(Nk**2,Nk**2),intent(in)::A
    real*8,dimension(Nk**2,Nk**2),intent(out)::L
    integer::i,j,k
    real*8::inter,inter1

    L=0.0

    L(1,1)=sqrt(abs(A(1,1)))

    do j=2,Nk**2
       L(j,1)=A(1,j)/L(1,1)
    end do

    do i=2,Nk**2
       inter=0.0
       do k=1,i-1
          inter = inter + (L(i,k))**2
       end do
       L(i,i)=sqrt( A(i,i) - inter)
       do j=i+1,Nk**2
          inter1=0.0
          do k=1,i-1
             inter1 = inter1 + L(i,k)*L(j,k)
          end do
          L(j,i)= (A(i,j) - inter1) / L(i,i)
       end do
    end do



  end subroutine cholesky


  subroutine descente(b,L,y)
    implicit none
    real*8,dimension(Nk**2),intent(in)::b
    real*8,dimension(Nk**2,Nk**2),intent(in)::L
    real*8,dimension(Nk**2),intent(out)::y
    integer::i,k
    real*8::inter

    y(1)=b(1)/L(1,1)

    do i=1, nk**2
       inter=0.0
       do k=1,i-1
          inter=inter + L(i,k)*y(k)
       end do
       y(i)=(b(i)-inter)/L(i,i)
    end do

  end subroutine descente

  subroutine remontee(L,Y,X)
    implicit none
    real*8,dimension(Nk**2,Nk**2),intent(in)::L
    real*8,dimension(Nk**2),intent(in)::Y
    real*8,dimension(Nk**2),intent(out)::X
    real*8,dimension(Nk**2,Nk**2)::P
    integer::i,k,j
    real*8::felix

    do i=1,Nk**2
       do j=1,Nk**2
          P(i,j)=L(j,i) !P est la transposée de L
       end do
    end do

    x(Nk**2) = Y(Nk**2) / P(Nk**2,Nk**2)

    do i=Nk**2-1,1,-1
       felix =0
       do k=i+1,Nk**2
          felix = felix + P(i,k)* x(k)
       end do
       x(i)=(y(i)-felix)/P(i,i)
    end do

  end subroutine remontee



end module Mu_Resol
