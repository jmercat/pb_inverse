module fonctions
use parametres
implicit none
contains

subroutine Init(X_part)
	implicit none
	real*8,intent(inout),dimension(:,:),allocatable::X_part
	real*8::x1,x2
	integer::k
	
	x1=-1./(Nk-1)
	x2=0

	do k=1,Nk**2
		x1=x1+1D0/(Nk-1)
		if (x1>1.) then
			x2=x2+1D0/(Nk-1)
			x1=x1-(1+1D0/(Nk-1))
		end if
		
	X_part(k,1)=x1
	X_part(k,2)=x2
		
	!print*,X_part(k,1),X_part(k,2)

	end do
end subroutine

!Affichage des fonctions
subroutine Affiche_Dirac(l)
	implicit none
	integer::i,j
	integer,intent(in)::l
	real*8,dimension(2)::Coord
	real*8,dimension(0:N)::X,Y
	open(unit=1, file = "Affichage", form='formatted',status='unknown',action='write')
	
	!Affichage sous forme matrice
	do j=0,N
		Coord(1)=j*1./N 	
		X(j)=Coord(1)
	end do
	write(1,*)0,X(:)
	
	do i=0,N
		do j=0,N
			Coord(1)=j*1./N
			Coord(2)=X(i)
			Y(j)=Dirac(Coord(:),X_part(l,:))
		end do
		write(1,*)X(i),Y(:)
	end do
	close(1)
	
	open(unit=2, file = "Commande", form='formatted',status='unknown',action='write')
	write(2,*)'set term png'
	write(2,*)'set output "Dirac.png"'
	write(2,*)'splot "Affichage" matrix nonuniform w pm3d'
	close(2)
	call system ('gnuplot Commande')
end subroutine

subroutine Affiche_Ddirac(l,m)
	implicit none
	integer::i,j
	integer,intent(in)::l,m
	real*8,dimension(2)::Coord
	real*8,dimension(0:N)::X,Y
	open(unit=1, file = "Affichage_ddirac", form='formatted',status='unknown',action='write')
	
	!Affichage sous forme matrice
	do j=0,N
		Coord(1)=j*1./N 	
		X(j)=Coord(1)
	end do
	write(1,*)0,X(:)
	
	do i=0,N
		do j=0,N
			Coord(1)=j*1./N
			Coord(2)=X(i)
			Y(j)=Ddirac(Coord(:),X_part(l,:),m)
		end do
		write(1,*)X(i),Y(:)
	end do
	close(1)
	
	open(unit=2, file = "Commande", form='formatted',status='unknown',action='write')
	write(2,*)'set term png'
	write(2,*)'set output "ddirac.png"'
	write(2,*)'splot "Affichage_ddirac" matrix nonuniform w pm3d'
	close(2)
	call system ('gnuplot Commande')
end subroutine

function dirac(x,xk)
	implicit none
	real*8,dimension(2)::x,xk
	real*8::dirac

	if (x(1)-xk(1)<-eps/2 .or. x(1)-xk(1)>eps/2 .or. x(2)-xk(2)<-eps/2 .or. x(2)-xk(2)>eps/2) then
		dirac = 0
	else
		dirac = 2D0/eps-4d0*max(abs(x(1)-xk(1)),abs(x(2)-xk(2)))/(eps**2)
	end if
	
end function


function ddirac(x,xk,l)
	implicit none
	real*8, dimension(2)::x,xk
	real*8::ddirac
	integer::l
	
	if (max(abs(x(1)-xk(1)),abs(x(2)-xk(2)))>eps/2) then
		ddirac = 0d0
	elseif (l==1) then
		if (x(1)-xk(1)<0 .and. abs(x(1)-xk(1))>abs(x(2)-xk(2))) then
			ddirac = 4d0/eps**2
		elseif (x(1)-xk(1)>0 .and. abs(x(1)-xk(1))>abs(x(2)-xk(2))) then
			ddirac = -4d0/eps**2
		else
			ddirac =0D0
		end if
	elseif (l==2) then
		if (x(2)-xk(2)<0 .and. abs(x(2)-xk(2))>abs(x(1)-xk(1))) then
			ddirac = 4d0/eps**2
		elseif (x(2)-xk(2)>0 .and. abs(x(2)-xk(2))>abs(x(1)-xk(1))) then
			ddirac = -4d0/eps**2
		else
			ddirac =0D0
		end if
	else
		print*, "Derivée du dirac selon une variable inexistante"
	end if
	
	
end function

function integralefdft(f,l,mu,m)
	implicit none
	
		interface
			real*8 function f(x)
				real*8,dimension(2)::x
			end function
		end interface

		integer :: i, j, k,l,m
		real*8::integralefdft,x(N),mu(Nk**2),dft
		
		integralefdft =0D0
		x(1)=0
	do i=2,N
		x(i)=x(i-1)+1d0/(N-1)
	end do
	do i=1,N
		do j=1,N
			dft = -ddirac((/x(i),x(j)/),X_part(l,:),m)*mu(l)
			integralefdft = integralefdft + (1D0/(N-1)**2)*f((/x(i),x(j)/))*dft
		end do
	end do

end function

function integraledftft(mu,l,m)
	implicit none

		integer :: i, j,k ,l,m
		real*8::integraledftft,x(N),mu(Nk**2),ft,dft
		
		integraledftft =0D0
		x(1)=0
	do i=2,N
		x(i)=x(i-1)+1d0/(N-1)
	end do
	do i=1,N
		do j=1,N
			dft = -ddirac((/x(i),x(j)/),X_part(l,:),m)*mu(l)
			ft=0d0
			do k = 1, Nk**2
				ft=ft+mu(k)*dirac((/x(i),x(j)/),X_part(k,:))
			end do
			integraledftft = integraledftft + 1D0/(N-1)**2*dft*ft
		end do
	end do

end function


function integraledftdft(mu,l1,l2,m)
	implicit none

		integer :: i, j,k ,l1,l2,m
		real*8::integraledftdft,dft1,dft2
		real*8,dimension(Nk**2)::mu
		real*8,dimension(N)::x
		real*8,dimension(2)::xi
		integraledftdft =0D0
		x(1)=0
	do i=2,N
		x(i)=x(i-1)+1d0/(N-1)
	end do
	do i=1,N
		do j=1,N
			xi(1) = x(i)
			xi(2) = x(j)
			dft1 = -ddirac(xi,X_part(l1,:),m)*mu(l1)
			dft2 = -ddirac(xi,X_part(l2,:),m)*mu(l2)
			integraledftdft = integraledftdft + 1D0/(N-1)**2*dft1*dft2
		end do
	end do
	!print*, "integraledftdft",integraledftdft
end function

subroutine buildmatA(mu,matA)
	implicit none
	real*8,dimension(2*Nk**2,2*Nk**2),intent(out)::matA
	real*8,dimension(Nk**2)::mu
	integer::i,j,m
	do i=1,Nk**2
		matA(i,i)= 2*integraledftdft(mu,i,i,1)+2*penalisation
	!	print*, matA(i,i)
		matA(i+Nk**2,i+Nk**2)= 2*integraledftdft(mu,i,i,2)+2*penalisation
		do j=i+1,Nk**2
			matA(i,j)= 2*integraledftdft(mu,i,j,1)
			matA(j,i)= matA(i,j)
			matA(i+Nk**2,j+Nk**2)= 2*integraledftdft(mu,i,j,2)
			matA(j+Nk**2,i+Nk**2)= matA(i+Nk**2,j+Nk**2)
		end do
	end do
end subroutine

function matmulF(matA,x) result(res)
	implicit none
	real*8,dimension(Nk**2)::x,res
	real*8,dimension(Nk**2,Nk**2)::matA
	real*8::dft1,dft2
	integer::i,j
	res=0
	do i=1,Nk**2
		do j=1,Nk**2
			res(i)=res(i)+x(j)*matA(i,j)
		end do
	end do
end function

subroutine gradconj(matA,x,b)
	implicit none
	real*8,dimension(2*Nk**2,2*Nk**2)::matA
	real*8, dimension(2*Nk**2)::b,x1
	real*8, dimension(Nk**2,2)::x
	real*8, dimension(2*Nk**2)::p,r,q,rp
	real*8::beta, alpha
	integer::m,i,k,compteur
	
	compteur = 0
	x1(1:Nk**2)=x(:,1)
	x1(Nk**2+1:2*Nk**2)=x(:,2)
	
	r = b-matmul(matA,x1)
	p = r
	rp= r
	do while(dot_product(r,r)>tol_gradconj**2 .and. compteur<itermax)
	
		compteur = compteur+1
		if (compteur>= itermax) then
			print*, "Gradient conjugué : Convergence non atteinte ! "
		endif

		q = matmul(matA,p)
		
		
		
		alpha = dot_product(r,r)/dot_product(p,q)
		x1 = x1 + alpha*p
		rp=r
		r = r - alpha*q
		beta = dot_product(r,r)/dot_product(rp,rp)
		p = r + beta*p
		!print*, "erreur gradconj", dot_product(r,r)
	end do	
	
	x(:,1)=x1(1:Nk**2)
	x(:,2)=x1(Nk**2+1:2*Nk**2)
	
end subroutine

subroutine solveXk(mu)
	implicit none
	real*8,dimension(2*Nk**2,2*Nk**2)::matA
	real*8,dimension(2*Nk**2)::b,dI
	real*8,dimension(Nk**2)::mu
	real*8,dimension(Nk**2,2)::xkp,x_partp
	real*8::test_arret
	real*8::arret,temp
	integer::i,compteur
	xkp =x_part
		
		arret = 10
	compteur = 0
	x_partp = 0
	test_arret=Get_Error2_Rho1_Rho0Tilde(Mu_i)
	!do while (arret>tol_gradconj .and. compteur < 100)
	do while (test_arret > Arret_Critere)
	test_arret=Get_Error2_Rho1_Rho0Tilde(Mu_i)
	print*,'Erreur L2 Rho1_Rho0_tilde',test_arret,'Arret_Critere',Arret_Critere
		
		call buildmatA(mu,matA)	
		do i=1,Nk**2
			dI(i) = -2*integralefdft(rho1,i,mu,1)+2*integraledftft(mu,i,1)-&
			&2*penalisation*(xkp(i,1)-X_part(i,1))
			dI(i+Nk**2) = -2*integralefdft(rho1,i,mu,2)+&
			&2*integraledftft(mu,i,2)-2*penalisation*(xkp(i,2)-X_part(i,2))
		end do 
		b = -dI
		call gradconj(matA,X_partp,b)
		x_part = x_partp+x_part
!		
!		arret =dot_product(x_partp(:,1),x_partp(:,1))
!		arret = arret + dot_product(x_partp(:,2),x_partp(:,2))
!		temp =dot_product(xkp(:,1),xkp(:,1))
!		temp =temp+dot_product(xkp(:,2),xkp(:,2))
!		
!		arret = arret/temp
		call Affiche_rho0_tilde()
		call system ('gnuplot Commande')
		print*, "etape : ", compteur
	end do
	
end subroutine

real*8 function rho1(x)
	implicit none
	real*8,dimension(2)::x	
	rho1 = x(1)+x(2)
end function

real*8 function rho0(x)
	implicit none
	real*8,dimension(2)::x	
	rho0 = 1-x(1)+x(2)
end function



!~ function rho1(X) result(r)
!~ implicit none
!~ real*8,dimension(2)::X
!~ real*8::r
!~ 
!~ if (x(1)<1 .and. x(2)<1) then
!~ 	r=exp(-((X(1)-0.5)/2)**2-((X(2)-0.5)/2)**2)
!~ else
!~ 	r=0
!~ end if
!~ 
!~ end function

subroutine Affiche_rho0()
	implicit none
	integer::i,j
	real*8,dimension(2)::Coord
	real*8,dimension(0:N)::X,Y
	open(unit=1, file = "Affichage_rho0", form='formatted',status='unknown',action='write')
	
	!Affichage sous forme matrice
	do j=0,N
		Coord(1)=j*1./N 	
		X(j)=Coord(1)
	end do
	write(1,*)0,X(:)
	
	do i=0,N
		do j=0,N
			Coord(1)=j*1./N
			Coord(2)=X(i)
			Y(j)=rho0(Coord(:))
		end do
		write(1,*)X(i),Y(:)
	end do
	close(1)
	
	open(unit=2, file = "Commande", form='formatted',status='unknown',action='write')
	write(2,*)'set term png'
	write(2,*)'set output "Affichage_rho0.png"'
	write(2,*)'splot "Affichage_rho0" matrix nonuniform w pm3d'
	close(2)
	call system ('gnuplot Commande')

end subroutine

function rho0_tilde(X,Mu_i) result(r)
implicit none
real*8,dimension(2),intent(in)::X
real*8,dimension(Nk),intent(in)::Mu_i
real*8::r
integer::i
r=0
do i=1,Nk**2
	r=r+Mu_i(i)*Dirac(X(:),X_part(i,:))
end do

end function

subroutine Affiche_rho0_tilde()
	implicit none
	integer::i,j
	real*8,dimension(2)::Coord
	real*8,dimension(0:N)::X,Y
	open(unit=1, file = "Affichage_rho0_tilde", form='formatted',status='unknown',action='write')
	
	!Affichage sous forme matrice
	do j=0,N
		Coord(1)=j*1./N 	
		X(j)=Coord(1)
	end do
	write(1,*)0,X(:)
	
	do i=0,N
		do j=0,N
			Coord(1)=j*1./N
			Coord(2)=X(i)
			Y(j)=rho0_tilde(Coord(:),Mu_i)
		end do
		write(1,*)X(i),Y(:)
	end do
	close(1)
	
	open(unit=2, file = "Commande", form='formatted',status='unknown',action='write')
	write(2,*)'set term png'
	write(2,*)'set output "Affichage_rho0_tilde.png"'
	write(2,*)'splot "Affichage_rho0_tilde" matrix nonuniform w pm3d'
	close(2)
	call system ('gnuplot Commande')

end subroutine

subroutine Get_Error2_Rho0_Rho0Tilde(Mu_i,normeL2) 
	implicit none
	real*8,dimension(2)::Coord
	real*8,dimension(Nk),intent(in)::Mu_i
	real*8::normeL2
	integer::l,k
	
	normeL2=0
    do l=1,N
       Coord(2)=l*1./N
       do k=1,N
          Coord(1)=k*1./N 
          normeL2=normeL2+(rho0_tilde(Coord(:),Mu_i)-rho0(Coord(:)))**2*1./N**2
       end do
    end do
    print*,'Erreur L2 Rho0_Rho0Tilde:',normeL2
end subroutine

function Get_Error2_Rho1_Rho0Tilde(Mu_i) result(r)
	implicit none
	real*8,dimension(2)::Coord
	real*8,dimension(Nk),intent(in)::Mu_i
	real*8::r
	integer::l,k
	
	
	r=0
    do l=1,N
       Coord(2)=l*1./N
       do k=1,N
          Coord(1)=k*1./N 
          r=r+(rho0_tilde(Coord(:),Mu_i)-rho1(Coord(:)))**2*1./N**2
       end do
    end do

end function

end module
